package utils

import (
	"fmt"
	"log"
	"os"
)

func PrintDay(dayNumber string) {
	fmt.Printf("-------------------------------- DAY %s ------------------------------------\n", dayNumber)
}

func LogError(dayNumber string, err error) {
	log.Fatalf("Day %s error %v", dayNumber, err)
}

func ReadInput(relativePath string) (string, error) {
	pwd, err := os.Getwd()
	if err != nil {
		return "", err
	}
	file, err := os.ReadFile(pwd + relativePath + ".txt")
	if err != nil {
		return "", err
	}
	return string(file), nil
}
