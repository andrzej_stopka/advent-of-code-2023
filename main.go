package main

import (
	"adventOfCode/solutions"
)

func main() {
	solutions.Day1Part1()
	solutions.Day1Part2()
	solutions.Day2Part1()
	solutions.Day2Part2()
	solutions.Day3Part1()
}
