package solutions

import (
	"adventOfCode/utils"
	"fmt"
	"strconv"
	"strings"
)

func Day1Part1() {
	dayNumber := "1"
	utils.PrintDay(dayNumber)
	input, err := utils.ReadInput("/inputs/1")
	if err != nil {
		utils.LogError(dayNumber, err)
	}
	var result1 int
	for _, line := range strings.Split(strings.TrimRight(input, "\n"), "\n") {
		var firstDigit, lastDigit string
		for _, character := range line {
			if character >= '0' && character <= '9' {
				if firstDigit == "" {
					firstDigit = string(character)
				}
				lastDigit = string(character)
			}
		}
		lineNumber, err := strconv.Atoi(firstDigit + lastDigit)
		if err != nil {
			utils.LogError(dayNumber, err)
		}
		result1 += lineNumber
	}
	fmt.Printf("Part 1 Result: %d\n", result1)
}

func Day1Part2() {
	dayNumber := "1"
	input, err := utils.ReadInput("/inputs/1")
	if err != nil {
		utils.LogError(dayNumber, err)
	}
	var result2 int
	digitsMap := map[string]string{
		"one":   "1",
		"two":   "2",
		"three": "3",
		"four":  "4",
		"five":  "5",
		"six":   "6",
		"seven": "7",
		"eight": "8",
		"nine":  "9",
	}

	for _, line := range strings.Split(strings.TrimRight(input, "\n"), "\n") {
		firstCharacterIndex := -1
		lastCharacterIndex := -1
		var firstDigit, lastDigit string
		for key, value := range digitsMap {
			firstIndex := strings.Index(line, key)
			lastIndex := strings.LastIndex(line, key)
			if firstIndex > -1 {
				if firstCharacterIndex < 0 {
					firstCharacterIndex = firstIndex
					firstDigit = value
				}
				if lastCharacterIndex < 0 {
					lastCharacterIndex = lastIndex
					lastDigit = value
				}
				if firstCharacterIndex > firstIndex {
					firstCharacterIndex = firstIndex
					firstDigit = value
				}
				if lastCharacterIndex < lastIndex {
					lastCharacterIndex = lastIndex
					lastDigit = value
				}
			}
		}
		for index, character := range line {
			if character >= '0' && character <= '9' {
				if firstDigit == "" || firstCharacterIndex > index {
					firstCharacterIndex = index
					firstDigit = string(character)
				}
				if lastCharacterIndex < index {
					lastCharacterIndex = index
					lastDigit = string(character)
				}
			}
		}
		lineNumber, err := strconv.Atoi(firstDigit + lastDigit)
		if err != nil {
			utils.LogError(dayNumber, err)
		}
		result2 += lineNumber
	}
	fmt.Printf("Part 2 Result: %d\n", result2)
}
