package solutions

import (
	"adventOfCode/utils"
	"fmt"
	"regexp"
	"strconv"
	"strings"
)

func Day2Part1() {
	dayNumber := "2"
	utils.PrintDay(dayNumber)
	input, err := utils.ReadInput("/inputs/2")
	if err != nil {
		utils.LogError(dayNumber, err)
	}
	var result1 int
	for _, line := range strings.Split(strings.TrimRight(input, "\n"), "\n") {
		colorMap := make(map[string]bool)
		colorChannel := make(chan map[string]bool, 3)
		defer close(colorChannel)

		go countCubesNumber(line, "red", colorChannel)
		go countCubesNumber(line, "green", colorChannel)
		go countCubesNumber(line, "blue", colorChannel)

		for i := 0; i < 3; i++ {
			color := <-colorChannel
			for k, v := range color {
				colorMap[k] = v
			}
		}

		if !colorMap["red"] || !colorMap["green"] || !colorMap["blue"] {
			continue
		}
		idRegex := regexp.MustCompile(`Game (\d+):`)
		gameID := idRegex.FindStringSubmatch(line)
		intGameID, err := strconv.Atoi(gameID[1])
		if err != nil {
			utils.LogError(dayNumber, err)
		}
		result1 += intGameID
	}
	fmt.Printf("Part 1 Result: %d\n", result1)
}

func Day2Part2() {
	dayNumber := "2"
	input, err := utils.ReadInput("/inputs/2")
	if err != nil {
		utils.LogError(dayNumber, err)
	}
	var result2 int
	for _, line := range strings.Split(strings.TrimRight(input, "\n"), "\n") {
		colorMap := make(map[string]int)
		colorChannel := make(chan map[string]int, 3)
		defer close(colorChannel)

		go getRequiredMinimum(line, "red", colorChannel)
		go getRequiredMinimum(line, "green", colorChannel)
		go getRequiredMinimum(line, "blue", colorChannel)

		for i := 0; i < 3; i++ {
			color := <-colorChannel
			for k, v := range color {
				colorMap[k] = v
			}
		}
		linePower := colorMap["red"] * colorMap["green"] * colorMap["blue"]
		result2 += linePower
	}

	fmt.Printf("Part 2 Result: %d\n", result2)

}

func countCubesNumber(line, color string, c chan map[string]bool) {
	regexPattern := fmt.Sprintf(`(\d+) %s`, color)
	re := regexp.MustCompile(regexPattern)
	matches := re.FindAllStringSubmatch(line, -1)
	limitMap := map[string]int{
		"red":   12,
		"green": 13,
		"blue":  14,
	}
	limit := limitMap[color]

	colorMap := make(map[string]bool)
	for _, match := range matches {
		intMatch, err := strconv.Atoi(match[1])
		if err != nil {
			utils.LogError("2", err)
			colorMap[color] = false
			c <- colorMap
			return
		}
		if intMatch > limit {
			colorMap[color] = false
			c <- colorMap
			return
		}
	}
	colorMap[color] = true
	c <- colorMap
}

func getRequiredMinimum(line, color string, c chan map[string]int) {
	regexPattern := fmt.Sprintf(`(\d+) %s`, color)
	re := regexp.MustCompile(regexPattern)
	matches := re.FindAllStringSubmatch(line, -1)
	colorMap := make(map[string]int)
	var highest int
	for _, match := range matches {
		intMatch, err := strconv.Atoi(match[1])
		if err != nil {
			utils.LogError("2", err)
			colorMap[color] = 0
			c <- colorMap
		}
		if intMatch > highest {
			highest = intMatch
		}
	}
	colorMap[color] = highest
	c <- colorMap
}
