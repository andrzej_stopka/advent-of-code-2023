package solutions

import (
	"adventOfCode/utils"
	"fmt"
	"regexp"
	"strconv"
	"strings"
	"unicode"
)

func Day3Part1() {
	dayNumber := "3"
	utils.PrintDay(dayNumber)
	input, err := utils.ReadInput("/inputs/3")
	if err != nil {
		utils.LogError(dayNumber, err)
	}
	var linesSlice []string
	for _, line := range strings.Split(strings.TrimRight(input, "\n"), "\n") {
		linesSlice = append(linesSlice, line)
	}

	var result int
	for lineIndex, line := range linesSlice {
		numbers := extractNumbers(line)
		handledNumbers := make(map[string]bool)
		for _, number := range numbers {
			_, ok := handledNumbers[number]
			if ok {
				continue
			}
			indexes := getIndexes(line, number)
			switch len(indexes) {
			case 1 :
				correctNumber := checkAdjacent(linesSlice, lineIndex, indexes[0][0]+1, indexes[0][1]-1)
				if correctNumber {
					increaseResult(&result, number)
				}
			case 2: {
				correctNumber := checkAdjacent(linesSlice, lineIndex, indexes[0][0]+1, indexes[0][1]-1)
				if correctNumber {
					increaseResult(&result, number)
				}
				correctNumber = checkAdjacent(linesSlice, lineIndex, indexes[1][0]+1, indexes[1][1]-1)
				if correctNumber {
					increaseResult(&result, number)
				}
				handledNumbers[number] = true
			}
			default:
					utils.LogError("3", fmt.Errorf("Another input, more than 2 same values in one line"))
					return
				}

			}
	}
	fmt.Printf("Part 1 Result: %d\n", result)
}

func extractNumbers(line string) []string {
	parts := strings.FieldsFunc(line, func(c rune) bool {
		return c < '0' || c > '9'
	})

	var numbers []string
	for _, part := range parts {
		if len(part) > 0 {
			numbers = append(numbers, part)
		}
	}
	return numbers
}

func getIndexes(line, number string) [][]int {
	rePattern := fmt.Sprintf(`(^|[^0-9])%s([^0-9]|$)`, number)
	re := regexp.MustCompile(rePattern)
	indexes := re.FindAllStringIndex(line, -1)
	return indexes
}

func increaseResult(result *int, number string) {
	intNumber, err := strconv.Atoi(number)
	if err != nil {
		utils.LogError("3", err)
		return
	}
	*result += intNumber
}

func checkAdjacent(linesSlice []string, lineIndex, beginIndex, endIndex int) bool {
	if lineIndex-1 >= 0 {
		upper := checkUpper(linesSlice, lineIndex, beginIndex, endIndex)
		if upper {
			return true
		}
	}
	
	beside := checkBeside(linesSlice, lineIndex, beginIndex, endIndex)
	if beside {
		return true
	}


	if lineIndex+1 < len(linesSlice) {
		lower := checkLower(linesSlice, lineIndex, beginIndex, endIndex)
		if lower {
			return true
		}
	}

	return false
}

func checkUpper(linesSlice []string, lineIndex, beginIndex, endIndex int) bool {
	var firstIndex, lastIndex int
	if beginIndex-1 < 0 {
		firstIndex = 0
	} else {
		firstIndex = beginIndex - 1
	}
	if endIndex+1 > len(linesSlice[lineIndex-1]) {

		lastIndex = len(linesSlice[lineIndex-1])
	} else {
		lastIndex = endIndex + 1
	}
	for _, char := range linesSlice[lineIndex-1][firstIndex:lastIndex] {
		if !unicode.IsDigit(char) && char != '.' {
			return true
		}
	}
	return false
}

func checkBeside(linesSlice []string, lineIndex, beginIndex, endIndex int) bool {
	var leftNeighbour, rightNeighbour byte
	if beginIndex > 0 {
		leftNeighbour = linesSlice[lineIndex][beginIndex-1]
	}
	if endIndex < len(linesSlice[lineIndex]) {
		rightNeighbour = linesSlice[lineIndex][endIndex]
	}

	if leftNeighbour != 0 {
		if !unicode.IsDigit(rune(leftNeighbour)) && string(leftNeighbour) != "." {
			return true
		}
	}

	if rightNeighbour != 0 {
		if !unicode.IsDigit(rune(rightNeighbour)) && string(rightNeighbour) != "." {
			return true
		}
	}
	return false
}

func checkLower(linesSlice []string, lineIndex, beginIndex, endIndex int) bool {
	var firstIndex, lastIndex int
		if beginIndex-1 < 0 {
			firstIndex = 0
		} else {
			firstIndex = beginIndex - 1
		}
		if endIndex+1 > len(linesSlice[lineIndex+1]) {
			lastIndex = len(linesSlice[lineIndex+1])
		} else {
			lastIndex = endIndex + 1
		}
		for _, char := range linesSlice[lineIndex+1][firstIndex:lastIndex] {
			if !unicode.IsDigit(char) && char != '.' {
				return true
			}
		}
	return false
}